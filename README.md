# SimpleApp - Node.js Application CI/CD Pipeline

This repository contains a basic Node.js application that reads names from a text file and outputs them on localhost. Additionally, a Continuous Integration and Continuous Deployment (CI/CD) pipeline has been set up using GitLab and Jenkins.

## Requirements
List of requirements for setting up and running the CI/CD pipeline.

## Jenkins Console Output
Detailed output from the Jenkins console during the execution of the CI/CD pipeline.

## GitLab Job Output
Detailed output from the GitLab console during the execution of the CI/CD pipeline.

## Stages Screenshot
Screenshots or visual representation of the different stages in the Jenkins pipeline.

## Localhost Output
Output of the Node.js application on localhost..

For detailed information on each of these subheadings, please refer to the corresponding sections below. 

---

### Requirements

1. **GitLab Repository:**
   - Create a new GitLab repository named "SimpleApp".
   - Initialize the repository with a simple README.md file.

2. **Sample Application:**
   - A simple standalone Node.js application is included (`app.js`).
   - The application reads names from a text file (`names.txt`) and outputs them on localhost.

3. **Jenkins Setup:**
   - Install Jenkins on your local machine or on a server of your choice.
   - Configure Jenkins to integrate with your GitLab repository.

4. **Create Jenkins Pipeline:**
   - A Jenkins pipeline has been created using a Jenkinsfile (`Jenkinsfile`).
   - The pipeline includes stages for:
     - Checkout: Checkout the source code from the GitLab repository.
     - Build: Build the Node.js application (installing npm dependencies).
     - Test: Execute linting on the application.
     - Deploy: Run the Node.js application.

5. **GitLab CI Configuration:**
   - A `.gitlab-ci.yml` file is included in the root of the GitLab repository.
   - GitLab CI is configured to trigger the Jenkins pipeline on each commit to the repository.

    **Configuring GitLab CI to Trigger Jenkins Pipeline**

Step 1: Overview

In this guide, we'll walk through the process of configuring GitLab CI to automatically trigger a Jenkins pipeline on each commit to the repository. This enables seamless integration between GitLab and Jenkins, ensuring that your Jenkins pipeline is triggered whenever changes are pushed to the GitLab repository.

Step 2: Prerequisites

Ensure you have a working Jenkins instance set up.
Install and configure ngrok to create a public URL for your local Jenkins server. You can get ngrok here.
Verify that ngrok is running and has generated a public URL for your Jenkins instance.

Step 3: GitLab Configuration

In your GitLab project, create a file named .gitlab-ci.yml at the root of your repository.

Add the following configuration to .gitlab-ci.yml:

stages:
  - trigger_jenkins_job

trigger_jenkins_job:
  stage: trigger_jenkins_job
  script:
    - curl -X POST https://your-ngrok-generated-url/job/YourJenkinsJobName/build?token=YourToken

Replace your-ngrok-generated-url with the ngrok URL pointing to your Jenkins instance, and replace YourJenkinsJobName and YourToken with your Jenkins job name and authentication token, respectively.

6. **Documentation:**
   - **Prerequisites:**
     - Node.js and npm installed.
     - GitLab account.
     - Jenkins installed and configured.
   - **Installation Steps:**
     - Clone the repository locally: `git clone <repository_url>`
     - Follow the Jenkins setup and configuration steps.
   - **Running the CI/CD Pipeline:**
     - Make a commit to the GitLab repository.
     - The pipeline should automatically trigger on GitLab CI and execute the Jenkins job.

7. **Testing:**
   - Test the CI/CD pipeline by making a commit to the GitLab repository.
   - Ensure that the pipeline executes successfully and deploys the application to the test environment.

---

### Jenkins Console Output

Refer Documentation PDF
---

### GitLab Job Output

Refer Documentation PDF
---

### Stages Screenshot

Refer Documentation PDF

---

### Localhost Output

Refer Documentation PDF

## Additional Notes
- The Jenkins job is triggered using a curl command in the `.gitlab-ci.yml` file.
- Jenkins is configured with a specific node (`my_pc`) for the pipeline.
- The application can be accessed on localhost after the deployment stage.

Feel free to reach out for any further clarification or assistance!
